<?php

namespace app\models;

use Yii;
use yii\web\IdentityInterface;

class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
   public static function tableName()
   {
		return 'user';
	}
	public function rules()
	{
		return [
			[['username', 'password', 'auth_key'], 'string', 'max' => 255],
			[['username', 'password'], 'required'],
			[['username'], 'unique']
		];
	}
	public function attributeLabels()
	{
		return[
			'id' => 'ID',
			'username' => 'Username',
			'password' => 'Password',
			'auth_key' => 'auth_key',
		];
	}

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
		return static::findOne($id);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('you can only login by username/password pair for now');
    }

    /**
     * Finds user by username
     *
     * @param  string      $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username]);
    }
	  public function beforeSave($insert)
    {
        $return = parent::beforeSave($insert);

        if ($this->isAttributeChanged('password'))
            $this->password = Yii::$app->security->
					generatePasswordHash($this->password);

        if ($this->isNewRecord)
		    $this->auth_key = Yii::$app->security->generateRandomString(32);

        return $return;
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey === $authKey;
    }
	public function getLeads()
	{
		return $this->hasMany(Lead::className(), ['ownerId' => 'id']);
	}

    /**
     * Validates password
     *
     * @param  string  $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password);
    }
}
